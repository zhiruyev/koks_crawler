import scrapy
from w3lib.html import remove_tags as w3_remove_tags


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    start_urls = [
        "https://pet-mir.ru",
        "https://landofgames.ru",
        "https://tophotels.ru"
    ]

    def parse(self, response):
        body = response.css("body").get()
        body_text = w3_remove_tags(body)
        filename = f"{response.url.split('/')[-1]}.txt"
        with open(filename, "w") as file:
            file.write(body_text)
